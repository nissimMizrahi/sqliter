#pragma once
#include <string>
#include <functional>
#include "json.hpp"
#include "sqlite3.h"

using namespace nlohmann;

#define NOOP_CB [](const json& row){}

class Sqliter
{
	typedef std::function<void(const json& row)> CB_function;

	sqlite3* db;

	static int callback(void* NotUsed, int argc, char** argv, char** azColName);

public:

	Sqliter(const std::string& path);
	Sqliter(Sqliter&& other);
	~Sqliter();

	void execute(const std::string& query, const CB_function& cb = NOOP_CB);
};

