#include "Sqliter.h"

int Sqliter::callback(void* cb_ptr, int argc, char** argv, char** azColName)
{
	int i;
	json ret;
	for (i = 0; i < argc; i++) {
		ret[azColName[i]] = argv[i] ? argv[i] : "NULL";
	}
	
	CB_function* cb = (CB_function*)cb_ptr;

	(*cb)(ret);

	return 0;
}

Sqliter::Sqliter(const std::string& path)
{
	int rc = sqlite3_open(path.c_str(), &db);

	if (rc) throw std::exception((std::string() + "cant open db" + sqlite3_errmsg(db)).c_str());
}

Sqliter::Sqliter(Sqliter&& other) : db(other.db)
{
	other.db = nullptr;
}

Sqliter::~Sqliter()
{
	if (db) sqlite3_close(db);
	db = nullptr;
}

void Sqliter::execute(const std::string& query, const CB_function& cb)
{
	if (!db) throw std::exception("no valid db");

	char* zErrMsg = 0;

	int rc = sqlite3_exec(db, query.c_str(), callback, (void*)& cb, &zErrMsg);

	if (rc != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw std::exception((std::string() + "SQL error: %s\n" + err).c_str());
	}
}
