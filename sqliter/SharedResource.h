#pragma once
#include <mutex>
#include <memory>

#include "SharedResourceGuard.h"

template<class T>
class SharedResourceGuard;

template<class T>
class SharedResource
{
	std::shared_ptr<std::mutex> mutex_pointer;
	std::shared_ptr<T> value;

	friend SharedResourceGuard<T>;

public:
	
	SharedResource(T&& val);
	SharedResource(T& val);
	~SharedResource();

	SharedResourceGuard<T> lock();
	bool is_locked();
};

template<class T>
inline SharedResource<T>::SharedResource(T&& val): mutex_pointer(std::make_shared<std::mutex>()), value(std::make_shared<T>(std::forward<T>(val)))
{
}

template<class T>
inline SharedResource<T>::SharedResource(T& val): mutex_pointer(std::make_shared<std::mutex>()), value(std::make_shared<T>(std::forward<T>(val)))
{
}

template<class T>
inline SharedResource<T>::~SharedResource()
{
}

template<class T>
inline SharedResourceGuard<T> SharedResource<T>::lock()
{
	return SharedResourceGuard<T>(*this);
}


template<class T>
bool SharedResource<T>::is_locked()
{
	if (this->mutex_pointer->try_lock())
	{
		this->mutex_pointer->unlock();
		return true;
	}
	return false;
}
