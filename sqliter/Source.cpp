#include <iostream>

#include "Sqliter.h"
#include <future>

#define ASYNC(x) std::async(std::launch::async, x)

int main()
{
	Sqliter db("bla.db");
	Sqliter db2("bla.db");

	//std::vector<std::future<void>> bla2;
	//
	//for (int i = 0; i < 1000; i++)
	//{
	//	bla2.emplace_back(std::async(std::launch::async, [&db, i]() {
	//		db.execute("insert into bla(id) values(" + std::to_string(i) + ")");
	//	}));
	//}

	
	std::vector<std::string> bla;
	db.execute("select * from bla", [&bla](auto& row) {
		std::string tmp = row["id"];
		bla.emplace_back(std::move(tmp));
	});

	for (auto& b : bla)
	{
		std::cout << b << std::endl;
	}

	return 0;
}